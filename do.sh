#!/bin/bash

case "$1" in
	'cleanup-install')
		rm -r /etc/vredenslack
		rm -r /var/lib/vredenslack

		cpanm .
	;;

	'init')
		vs-pm config init --config-file /etc/vredenslack/pm/config --base-folder /var/lib/vredenslack/pm --slackware-version 14.1
	;;

	'config')
		vs-pm config show --slackware-version 14.1
	;;

	'info')
		vs-pm --slackware-version 14.1 info geany
	;;

	'prepare-release')
		set -e
		pod2markdown scripts/vs-pm README.md
		./Build distclean
	;;

	*)
		echo "
Usage: $0 <action>
Actions:
  cleanup-install
  init
  config
  info
  prepare-release
  (increase-version)
  (increase-dbversion)
"
	;;
esac
