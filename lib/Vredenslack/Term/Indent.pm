package Vredenslack::Term::Indent;

use 5.010;
use strict;
use warnings;

use Carp;
use Exporter;
use Term::ANSIColor ':constants';

our @ISA = qw(Exporter);

our @EXPORT_OK = ( qw(indent unindent indentation printline) );

our @EXPORT = qw();

our $VERSION = 0.1;

my $indent = 0;
my $indent_str = '';
my $indent_chr = '  ';
my $hide = 0;

sub indent {
	$indent_str = $indent_chr x ++$indent;
}

sub unindent {
	return if $indent == 0;
	$indent_str = $indent_chr x --$indent;
}

sub indentation {
	$indent_str;
}

sub set_indent_char {
	$indent_chr = shift || croak 'no char provided';
	$indent_str = $indent_chr x $indent;
}

sub set_hide { $hide = shift }

sub set_indent {
	$indent = shift;
	$indent_str = $indent_chr x $indent;
}

sub printline {
	print $indent_str, @_, "\n" unless $hide;
}

1;

__END__

=head1 NAME

Vredenslack::Term::Indent - Terminal print line indentation tools

=head1 SYNOPSIS

	use Vredenslack::Term::Indent qw/indent unindent indentation printline/;

	print indent, 'this line is indented by 2 spaces', "\n";
	printline 'this line is indented by 2 spaces also and the linefeed is automatically written;
	print unindent, 'this line is not indented at all', "\n";
	indent; # simple identation
	print indentation, 'this line is indented by 2 spaces', "\n";

=head1 METHODS

=head2 indent (value)

Increases the internal indentation value and returns the indentation string.

=head2 unindent

Decreases the internal indentation value and returns the indentation string.

=head2 indentation

Returns the indentation string.

=head2 set_indent_char ($char)

Sets the character(s) used for indentation. Note that this can be a string, but it will be concatenated B<indent> number of times. Default is '  ' (2 spaces).

=head2 set_hide (hide)

If set to true then printline will not print anything to stdout. Use this to provide a I<quiet> feature to your command line tool, for example. Requires that all output uses the 'printline' function which is not always viable.

=head2 set_indent (value)

Use this to reset the indent to the specified value. Returns the indentation string.

=head1 SEE_ALSO

L<Indent> - This module is similar in concept but has dependencies to non core perl modules and it's not OO.

=head1 AUTHOR

JB Ribeiro (Vredens) - E<lt>vredens@gmail.comE<gt>

=head1 COPYRIGHT AND LICENSE

Copyright 2013-2016 JB Ribeiro.

This program is free software; you can redistribute
it and/or modify it under the same terms as Perl itself.

The full text of the license can be found in the
LICENSE file included with this module.

=cut
