package Vredenslack::PackageManager::Repository;

use v5.12;
use warnings;
use strict;

use Carp;
use Cwd qw/getcwd realpath/;
use Data::Dumper;
use File::Basename;
use File::Copy;
use File::Find;
use File::Path qw/make_path/;
use File::Spec;
use Term::ANSIColor ':constants';

use Vredenslack::Term::Indent qw/indent unindent indentation/;

#===============================================================================
sub new {
#===============================================================================
	my $class = shift;

	my ($name, $url, $root, $version, $priority) = @_;

	croak 'no name provided' unless $name;
	croak 'no version provided' unless $version;
	croak "invalid version ($version)" unless $version =~ /^\d{2}\.\d{1,2}$/ or $version eq 'all';

	$priority = 1 unless defined $priority;
	croak 'invalid priority value' unless $priority =~ /^[0-9]+$/;

	croak 'no repository URL provided' unless $url;
	my $type;
	if ($url =~ m!^rsync://!) {
		$type = 'rsync';
	} elsif ($url =~ m!^git@! or $url =~ m!^https://bitbucket\.org/! or $url =~ m!^https://github\.com/!) {
		$type = 'git';
	} elsif ($url =~ m!^https?://!) {
		$type = 'url';
	} elsif (-d $url) {
		$type = 'fs';
	} else {
		croak 'invalid repository URL';
	}

	croak 'no root folder for repository provided' unless $type eq 'fs' or $root;

	my $self = {
		name => $name,
		url  => $url,
		root => $type eq 'fs' ? $url : $root,
		version => $version,
		type => $type,
		priority => $priority,
	};

	if ($type eq 'rsync') {
		$self->{excludes} = {};
	}

	bless $self, $class;
}

#===============================================================================
sub name {
#===============================================================================
	my $self = shift;

	$self->{name};
}

#===============================================================================
sub priority {
#===============================================================================
	my $self = shift;
	my $priority = shift;

	if (defined $priority) {
		croak 'invalid priority value' unless $priority =~ /^[0-9]+$/;
		$self->{priority} = $priority;
	}

	$self->{priority};
}

#===============================================================================
sub type {
#===============================================================================
	my $self = shift;

	$self->{type};
}

#===============================================================================
sub url {
#===============================================================================
	my $self = shift;

	$self->{url};
}

#===============================================================================
sub folder {
#===============================================================================
	my $self = shift;

	return $self->{url} if $self->{type} eq 'fs';

	File::Spec->catfile($self->{root}, $self->{name});
}

#===============================================================================
sub version {
#===============================================================================
	my $self = shift;

	$self->{version};
}

#===============================================================================
sub fix {
#===============================================================================
	my $self = shift;
	my $fixed = 0;

	if ($self->{type} eq 'rsync' and not defined $self->{excludes}) {
		$self->{excludes} = {};
		$fixed = 1;
	}

	# priority added on v0.4.3
	unless (defined $self->{priority}) {
		$self->{priority} = 1;
		$fixed = 1;
	}

	$fixed;
}

#===============================================================================
sub update {
#===============================================================================
	my $self = shift;

	my $type = $self->{type};
	my $folder = $self->folder;
	my $e;

	my $basefolder = dirname($folder);
	unless (-d $basefolder) {
		print indentation, 'creating repositories\' base folder [', $basefolder, ']', "\n";
		make_path $basefolder or croak "Failed to create repositories' root folder [$basefolder]." ;
	}

	print indentation, 'Updating repository ', YELLOW, $self->{name}, RESET, "\n";
	if ($type eq 'rsync') {
		# TODO: rsync
		my $filters = '';
		for my $pkg_path (keys %{$self->{excludes}}) {
			$filters .= "--exclude='$pkg_path' ";
		}

		indent;
		print indentation, 'running ', CYAN, "rsync -az $filters --perms $self->{url} $folder...", RESET, "\n";
		`rsync -az $filters --perms $self->{url}/ $folder/`;
		$e = $!;
	} elsif ($type eq 'git') {
		indent;
		if (-d $folder) {
			print indentation, 'pulling changes from git repository...', "\n";
			my $cwd = getcwd;
			chdir $folder;
			`git pull`;
			$e = $!;
			chdir $cwd;
		} else {
			print indentation, 'clonning git repository...', "\n";
			`git clone -q $self->{url} $folder`;
			$e = $!;
		}
	} elsif ($type eq 'url') {
		# TODO: URL mirror
	}

	if ($e) {
		print indentation, RED, 'failed to update repository: ', $e, RESET, "\n";
	} else {
		print indentation, GREEN, 'done', RESET, "\n";
	}
	unindent;

	! $e;
}

1;

__END__

=head1 NAME

Vredenslack::PackageManager::Repository

=head1 SYNOPSIS

=head1 DESCRIPTION

=head1 METHODS

=head2 new($name, $url, $root, $version)

Creates a new repository.

=head2 folder()

Returns the repository folder location.

=head2 name()

Returns the repository name.

=head2 priority()

Returns the repository's priority value.

=head2 priority($priority)

Sets the repository's priority value to that number. Returns the repository's priority value.

=head2 type()

Returns the repository type.

=head2 update()

Updates the repository. If it is an rsync repository then runs an rsync from the remote server. Note that files in the local copy will be removed/overwritten from the remote server. If it is a github then a C<git reset --hard> is done before pulling changes from the remote server. Nothing is done if the repository is of the I<local folder> type.

=head2 url()

Returns the repository URL address.

=head2 validate()

Validates the repository.

=head2 version()

Returns the slackware version this repository is destined for. It can also return C<all> if it is destined for all slackware versions.

=head1 AUTHOR

JB Ribeiro (Vredens) - E<lt>vredens@gmail.comE<gt>

=head1 COPYRIGHT AND LICENSE

Copyright 2013-2016 JB Ribeiro.

This program is free software; you can redistribute
it and/or modify it under the same terms as Perl itself.

The full text of the license can be found in the
LICENSE file included with this module.

=cut
